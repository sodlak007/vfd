$(document).foundation();

$(document).ready(function(){
  $('.menu-link').click(function(){
    $(this).toggleClass('open');
    $('.main-dropdown').fadeToggle();
  });

  $('.show-products').click(function(e){
    e.preventDefault();
    $('.submenu-drp').slideToggle();
  });


  // $('.close-menu').click(function(){
  //   $('.mobile-menu').fadeToggle();
  // });

  $('.name-profile').click(function(e){
    e.stopPropagation();
    $('.dash-menu').fadeToggle();
  });

  $('.question').click(function(){
    $(this).next().slideToggle();
  });

  $('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
  });

  $("#example_id").ionRangeSlider({
    min: 100000,
    max: 2000000,
    from: 200000,
    to: 1800,
    hide_min_max: true,
    hide_from_to: false,
    grid: false,
    prefix: "₦ "
  });

  $("#months").ionRangeSlider({
    min: 1,
    max: 6,
    from: 2,
    postfix: " Month(s)"
  });

  $( "#from" ).datepicker();
  $( "#dob" ).datepicker();
  $( "#id" ).datepicker();


  function setHeight() {
    windowHeight = $(window).innerHeight();
    $('.height-match').css('min-height', windowHeight);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });

  var options = {};
  $('#dob, #sal, #fr-date').datepicker(options);
});

 $(document).click(function(){
    $('.dash-menu').fadeOut(300);
  });


var media = $('#id_media, #id_media1, #id_media2, #id_media3');
  if (media.length) {
      var mediaDefaultValue = $('.file span.value').text();
      var mediaCharLimit = 20;

      $('.file .bt-value').click(function(){
          media.click();
      });

      media.on('change', function() {
          var value = this.value.replace("C:\\fakepath\\", "");
          var newValue;
          var valueExt;
          var charLimit;

          if (value) {
              newValue = value;
              valueExt = value.split('.').reverse()[0];
              if (newValue.length > mediaCharLimit) {
                  charLimit = mediaCharLimit - valueExt.length;

                  // truncate chars.
                  newValue = $.trim(value).substring(0, charLimit) + '…';

                  // if file name has extension, add it to newValue.
                  if (valueExt.length) {
                      newValue += valueExt;
                  }
              }
          }
          else {
        newValue = mediaDefaultValue;
      }
    $(this).parent().find('span.value').text(newValue);
  });
}