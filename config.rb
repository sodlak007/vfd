http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"
relative_assets = true;

# You can select your preferred output style here (can be overridden via the command line):
output_style = :expanded  #:compressed 
#After dev :compressed

environment = :development
#environment = :production