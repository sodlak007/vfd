
	window.onload = function () {
		var chart = new CanvasJS.Chart("chartContainer",
		{
			theme: "theme3",
                        animationEnabled: true,
			title:{
				text: "",
				fontSize: 30
			},
			toolTip: {
				shared: true
			},			
			axisY: {
				title: "Amount",
				labelFontSize: 12,
				titleFontFamily: "GothamHTF-Book",
				labelFontFamily: "GothamHTF-Book"
			},
			axisX: {
				labelFontFamily: "GothamHTF-Book",
				labelFontSize: 12
			},
			theme: "theme2",		
			data: [ 
			{
				type: "column",	
				name: "Income",
				color: "darkgreen",
				legendText: "Income",
				showInLegend: true, 
				dataPoints:[
				{label: "Jan", y: 262},
				{label: "Feb", y: 211},
				{label: "March", y: 175},
				{label: "Apr", y: 137}


				]
			},
			{
				type: "column",	
				name: "Expense",
				legendText: "Expenses",
				color: "red",
				axisYType: "secondary",
				showInLegend: true,
				dataPoints:[
				{label: "Jan", y: 11.15},
				{label: "Feb", y: 2.5},
				{label: "Mar", y: 3.6},
				{label: "Apr", y: 4.2}


				]
			}
			
			],
          legend:{
            cursor:"pointer",
            fontSize: 12,
            fontFamily: "GothamHTF-Book",
            itemclick: function(e){
              if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              	e.dataSeries.visible = false;
              }
              else {
                e.dataSeries.visible = true;
              }
            	chart.render();
            }
          },
        });

chart.render();
}
